app.config(function($stateProvider, $urlRouterProvider, $authProvider,baseUrl,$httpProvider) {


  $authProvider.baseUrl=baseUrl;



  /**
   * Helper auth functions
   */
  var skipIfLoggedIn = function($q, $auth) {
    var deferred = $q.defer();
    if ($auth.isAuthenticated()) {
      deferred.reject();
    } else {
      deferred.resolve();
    }
    return deferred.promise;
  };

  var loginRequired = function($q, $location, $auth) {
    var deferred = $q.defer();
    if ($auth.isAuthenticated()) {
      deferred.resolve();
    } else {
      $location.path('/login');
    }
    return deferred.promise;
  };

  /**
   * App routes
   */
  $stateProvider
    .state('home', {
      url: '/',
      controller: 'HomeCtrl',
      templateUrl: 'partials/home.html',
      resolve: {
        loginRequired: loginRequired
      }
    })
    .state('login', {
      url: '/login',
      templateUrl: 'partials/login.html',
      controller: 'LoginCtrl',
      resolve: {
        skipIfLoggedIn: skipIfLoggedIn
      }
    })
    .state('signup', {
      url: '/signup',
      templateUrl: 'partials/signup.html',
      controller: 'SignupCtrl',
      resolve: {
        skipIfLoggedIn: skipIfLoggedIn
      }
    })
    .state('logout', {
      url: '/logout',
      template: null,
      controller: 'LogoutCtrl'
    })
    .state('profile', {
      url: '/profile',
      templateUrl: 'partials/profile.html',
      controller: 'ProfileCtrl',
      resolve: {
        loginRequired: loginRequired
      }
    });
  $urlRouterProvider.otherwise('/login');

  /**
   *  Satellizer config
   */
  $authProvider.facebook({
    clientId: '541576749376476'
  });

  $authProvider.google({
    clientId: '715966008555-1pque82k6j3ae1kgonpc5b3tu0jpgj86.apps.googleusercontent.com'
  });

});
