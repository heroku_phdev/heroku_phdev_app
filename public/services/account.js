angular.module('MyApp')
  .factory('Account', function($http,baseUrl) {
    return {
      getProfile: function() {
        var url=baseUrl+'/api/me';
        return $http.get(url);
      },
      updateProfile: function(profileData) {
        var url=baseUrl+'/api/me';
        return $http.put(url, profileData);
      }
    };
  });
