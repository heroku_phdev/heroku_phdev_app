module.exports = {
  // App Settings
  MONGO_URI: process.env.MONGO_URI,
  TOKEN_SECRET: process.env.TOKEN_SECRET,

  // OAuth 2.0
  GOOGLE_SECRET: process.env.GOOGLE_SECRET ,
  FACEBOOK_SECRET: process.env.FACEBOOK_SECRET
};
